import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:full_going_admin_app/components/common/component_appbar_popup.dart';
import 'package:full_going_admin_app/components/common/component_custom_loading.dart';
import 'package:full_going_admin_app/components/common/component_margin_vertical.dart';
import 'package:full_going_admin_app/components/common/component_notification.dart';
import 'package:full_going_admin_app/components/common/component_text_btn.dart';
import 'package:full_going_admin_app/config/config_color.dart';
import 'package:full_going_admin_app/config/config_form_formatter.dart';
import 'package:full_going_admin_app/config/config_form_validator.dart';
import 'package:full_going_admin_app/config/config_size.dart';
import 'package:full_going_admin_app/config/config_style.dart';
import 'package:full_going_admin_app/enums/enum_size.dart';
import 'package:full_going_admin_app/model/password_change_request.dart';
import 'package:full_going_admin_app/repository/repo_member.dart';
import 'package:full_going_admin_app/styles/style_form_decoration_full_going.dart';

class PageProfileFix extends StatefulWidget {
  const PageProfileFix({super.key});

  @override
  State<PageProfileFix> createState() => _PageProfileFixState();
}

class _PageProfileFixState extends State<PageProfileFix> {
  final _formKey = GlobalKey<FormBuilderState>();
  bool _isVisible = true;
  bool _isReVisible = true;
  bool _isCurrentVisible = true;

  Future<void> _doPasswordChange(
      PasswordChangeRequest passwordChangeRequest) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().doPasswordChange(passwordChangeRequest).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '비밀번호 변경 완료',
        subTitle: '비밀번호가 변경 되었습니다.',
      ).call();

      Navigator.pop(context);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '비밀번호 변경 실패',
        subTitle: '비밀번호가 변경을 실패하였습니다. 다시 확인해주세요',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: "비밀번호 변경"),
      body: SingleChildScrollView(
          child: Container(
        padding: bodyPaddingLeftRight,
        child: FormBuilder(
          key: _formKey,
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
            const ComponentMarginVertical(enumSize: EnumSize.big),
            Icon(
              Icons.report_problem_outlined,
              size: 150,
              color: colorRed,
              shadows: <Shadow>[
                Shadow(color: colorGray, blurRadius: 2, offset: Offset(0, 5))
              ],
            ),
            const ComponentMarginVertical(),
            Text(
              "비밀번호를 변경해 주세요!",
              style: TextStyle(
                  fontSize: fontSizeSuper,
                  fontWeight: FontWeight.bold,
                  color: colorRed),
            ),
            const ComponentMarginVertical(enumSize: EnumSize.mid),
            Text("주기적인 비밀번호 변경을 통해 개인정보를 안전하게 보호하세요."),
            const ComponentMarginVertical(enumSize: EnumSize.small),
            Text("아래에 새 비밀번호를 입력해 주세요."),
            const ComponentMarginVertical(enumSize: EnumSize.mid),
            FormBuilderTextField(
              keyboardType: TextInputType.text,
              name: 'currentPassword',
              inputFormatters: [maskNoInputKrFormatter],
              maxLength: 20,
              obscureText: _isCurrentVisible,
              decoration:
                  StyleFormDecorationOfFullGoing().getInputDecorationOfPassword(
                "현재 비밀번호",
                GestureDetector(
                  child: _isCurrentVisible
                      ? Icon(Icons.visibility_off,
                          size: sizeSuffixIconOfPassword, color: colorPrimary)
                      : Icon(Icons.visibility,
                          size: sizeSuffixIconOfPassword, color: colorPrimary),
                  onTap: () {
                    setState(() {
                      _isCurrentVisible = !_isCurrentVisible;
                    });
                  },
                ),
              ),
              validator: validatorOfLoginPassword,
            ),
            const ComponentMarginVertical(),
            FormBuilderTextField(
              keyboardType: TextInputType.text,
              name: 'newPassword',
              inputFormatters: [maskNoInputKrFormatter],
              maxLength: 20,
              obscureText: _isVisible,
              decoration:
                  StyleFormDecorationOfFullGoing().getInputDecorationOfPassword(
                "새 비밀번호",
                GestureDetector(
                  child: _isVisible
                      ? Icon(Icons.visibility_off,
                          size: sizeSuffixIconOfPassword, color: colorPrimary)
                      : Icon(Icons.visibility,
                          size: sizeSuffixIconOfPassword, color: colorPrimary),
                  onTap: () {
                    setState(() {
                      _isVisible = !_isVisible;
                    });
                  },
                ),
              ),
              validator: validatorOfLoginPassword,
            ),
            const ComponentMarginVertical(),
            FormBuilderTextField(
              keyboardType: TextInputType.text,
              name: 'newRePassword',
              inputFormatters: [maskNoInputKrFormatter],
              maxLength: 20,
              obscureText: _isReVisible,
              decoration:
                  StyleFormDecorationOfFullGoing().getInputDecorationOfPassword(
                "비밀번호 확인",
                GestureDetector(
                  child: _isReVisible
                      ? Icon(Icons.visibility_off,
                          size: sizeSuffixIconOfPassword, color: colorPrimary)
                      : Icon(Icons.visibility,
                          size: sizeSuffixIconOfPassword, color: colorPrimary),
                  onTap: () {
                    setState(() {
                      _isReVisible = !_isReVisible;
                    });
                  },
                ),
              ),
              validator: (value) =>
                  _formKey.currentState?.fields['newPassword']?.value != value
                      ? formErrorEqualPassword
                      : null,
            ),
            const ComponentMarginVertical(),
            Container(
              width: MediaQuery.of(context).size.width,
              child: ComponentTextBtn(
                text: "변경완료",
                textColor: Colors.white,
                borderColor: colorPrimary,
                callback: () {
                  if (_formKey.currentState!.saveAndValidate()) {
                    PasswordChangeRequest passwordChangeRequest =
                    PasswordChangeRequest(
                      _formKey.currentState!.fields['currentPassword']!.value,
                      _formKey.currentState!.fields['newPassword']!.value,
                      _formKey.currentState!.fields['newRePassword']!.value,
                    );
                    _doPasswordChange(passwordChangeRequest);
                  }
                },
              ),
            ),
          ]),
        ),
      )),
    );
  }
}
