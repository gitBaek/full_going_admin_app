import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:full_going_admin_app/components/common/component_appbar_actions.dart';
import 'package:full_going_admin_app/components/common/component_appbar_popup.dart';
import 'package:full_going_admin_app/components/common/component_custom_loading.dart';
import 'package:full_going_admin_app/components/common/component_margin_vertical.dart';
import 'package:full_going_admin_app/components/common/component_notification.dart';
import 'package:full_going_admin_app/components/common/component_text_btn.dart';
import 'package:full_going_admin_app/config/config_color.dart';
import 'package:full_going_admin_app/config/config_form_formatter.dart';
import 'package:full_going_admin_app/config/config_form_validator.dart';
import 'package:full_going_admin_app/config/config_style.dart';
import 'package:full_going_admin_app/enums/enum_size.dart';
import 'package:full_going_admin_app/model/admin_kickboard/kick_board_request.dart';
import 'package:full_going_admin_app/repository/repo_kick_board.dart';
import 'package:full_going_admin_app/styles/style_form_decoration.dart';
import 'package:full_going_admin_app/styles/style_form_decoration_full_going.dart';
import 'package:intl/intl.dart';

class PageKickBoard extends StatefulWidget {
  const PageKickBoard(
      {super.key});

  @override
  State<PageKickBoard> createState() => _PageKickBoardState();
}

class _PageKickBoardState extends State<PageKickBoard> {
  final _formKey = GlobalKey<FormBuilderState>();

  /*
  "dateBuy": "string",
  "modelName": "string",
  "posX": 0,
  "posY": 0,
  "priceBasis": "BASIC"
  */
  Future<void> _setKickBoard(KickBoardRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoKickBoard().setKickBoard(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '킥보드 등록 완료',
        subTitle: '킥보드 등록이 완료되었습니다.',
      ).call();
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '킥보드 등록 실패',
        subTitle: '입력값을 확인해주세요.',
      ).call();
    });
  }



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: "킥보드 등록"),
      body: SingleChildScrollView(
        child: Container(
          padding: bodyPaddingLeftRight,
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const ComponentMarginVertical(enumSize: EnumSize.big),
                FormBuilderTextField(
                  name: 'modelName',
                  decoration: StyleFormDecorationOfFullGoing()
                      .getInputDecoration('모델명'),
                  maxLength: 20,
                  keyboardType: TextInputType.text,
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                    FormBuilderValidators.minLength(3,
                        errorText: formErrorMinLength(3)),
                    FormBuilderValidators.maxLength(20,
                        errorText: formErrorMaxLength(20)),
                  ]),
                ),
                const ComponentMarginVertical(),
                FormBuilderDropdown<String>(
                  name: 'priceBasis',
                  initialValue: 'BASIC',
                  decoration: InputDecoration(
                    labelText: '가격기준',
                    filled: true,
                    isDense: false,
                    border: const OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                  ),
                  onChanged: (value) {
                    setState(() {});
                  },
                  items: const [
                    DropdownMenuItem(value: "BASIC", child: Text('베이직')),
                    DropdownMenuItem(value: "PREMIUM", child: Text('프리미엄')),
                  ],
                ),
                const ComponentMarginVertical(),
                FormBuilderDateTimePicker(
                    name: "dateBuy",
                    initialEntryMode: DatePickerEntryMode.calendarOnly,
                    inputType: InputType.date,
                    format: DateFormat('yyyy-MM-dd'),
                    decoration: InputDecoration(
                      labelText: '구입일',
                      filled: true,
                      isDense: false,
                      border: const OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                    )),
                const ComponentMarginVertical(),
                FormBuilderTextField(
                  name: 'posX',
                  decoration:
                      StyleFormDecorationOfFullGoing().getInputDecoration('위도'),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                  ]),
                ),
                const ComponentMarginVertical(),
                FormBuilderTextField(
                  name: 'posY',
                  decoration:
                      StyleFormDecorationOfFullGoing().getInputDecoration('경도'),
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(
                        errorText: formErrorRequired),
                  ]),
                ),
                ComponentMarginVertical(enumSize: EnumSize.big),
                    Container(
                        width: MediaQuery.of(context).size.width,
                        child: ComponentTextBtn(
                            text: '등록',
                            callback: () {
                              if (_formKey.currentState!.saveAndValidate()) {
                                KickBoardRequest kickBoardRequest =
                                    KickBoardRequest(
                                        _formKey.currentState!
                                            .fields['modelName']!.value,
                                        _formKey.currentState!
                                            .fields['priceBasis']!.value,
                                        maskDataFormatter.format(_formKey
                                            .currentState!
                                            .fields['dateBuy']!
                                            .value),
                                        double.parse(_formKey.currentState!
                                            .fields['posX']!.value),
                                        double.parse(_formKey.currentState!
                                            .fields['posY']!.value));

                                print(_formKey
                                    .currentState!.fields['modelName']!.value);
                                print(maskDataFormatter.format(_formKey
                                    .currentState!.fields['dateBuy']!.value));
                                print(_formKey
                                    .currentState!.fields['priceBasis']!.value);
                                print(_formKey
                                    .currentState!.fields['posX']!.value);
                                print(_formKey
                                    .currentState!.fields['posY']!.value);

                                _setKickBoard(kickBoardRequest);
                              }
                            }),
                      ),
                const ComponentMarginVertical(),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: ComponentTextBtn(
                      text: '취소',
                      bgColor: colorSecondary,
                      borderColor: colorSecondary,
                      callback: () {
                        Navigator.pop(context);
                      }),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
