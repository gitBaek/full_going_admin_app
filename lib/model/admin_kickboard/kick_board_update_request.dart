class KickBoardUpdateRequest {
  String modelName;
  String memo;
  double posX;
  double posY;
  String priceBasis;

  KickBoardUpdateRequest(this.modelName, this.priceBasis, this.posX, this.posY,
      this.memo);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['modelName'] = modelName;
    data['priceBasis'] = priceBasis;
    data['posX'] = posX;
    data['posY'] = posY;
    data['memo'] = memo;

    return data;
  }
}
