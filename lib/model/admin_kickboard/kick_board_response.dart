class KickBoardResponse{
  String modelName;
  double posX;
  double posY;
  String priceBasis;
  String kickBoardStatusName;
  String? memo;

  KickBoardResponse(this.modelName, this.posX, this.posY, this.priceBasis, this.kickBoardStatusName, this.memo);

  /*
   "kickBoardStatusName": "string",
    "memo": "string",
    "modelName": "string",
    "posX": 0,
    "posY": 0,
    "priceBasis": "BASIC"
    */


  factory KickBoardResponse.fromJson(Map<String, dynamic> json) {
    return KickBoardResponse(
      json['modelName'],
      json['posX'],
      json['posY'],
      json['priceBasis'],
      json['kickBoardStatusName'],
      json['memo']
    );
  }
}