class AuthCheckRequest {
  String phoneNumber;
  String authNum;

  AuthCheckRequest(this.phoneNumber, this.authNum);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['phoneNumber'] = this.phoneNumber;
    data['authNum'] = this.authNum;

    return data;
  }
}