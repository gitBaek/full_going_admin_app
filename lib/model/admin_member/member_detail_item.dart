class MemberDetailItem {
  String dateCreate;
  bool isLicence;
  String name;
  String phoneNumber;
  num remainMileage;
  num remainPass;
  num remainPrice;
  String username;

  MemberDetailItem(this.dateCreate, this.isLicence, this.name, this.phoneNumber, this.remainMileage, this.remainPass, this.remainPrice, this.username);

  factory MemberDetailItem.fromJson(Map<String, dynamic> json) {
    return MemberDetailItem(
      json['dateCreate'],
      json['isLicence'],
      json['name'],
      json['phoneNumber'],
      json['remainMileage'],
      json['remainPass'],
      json['remainPrice'],
      json['username'],
    );
  }
}
