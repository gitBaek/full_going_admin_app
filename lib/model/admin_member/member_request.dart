class MemberRequest {
  String memberGroup;
  String username;
  String password;
  String passwordRe;
  String name;
  String phoneNumber;
  String? licenceNumber;

  MemberRequest(this.memberGroup, this.username, this.password, this.passwordRe,
      this.name, this.phoneNumber, this.licenceNumber);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['memberGroup'] = memberGroup;
    data['username'] = username;
    data['password'] = password;
    data['passwordRe'] = passwordRe;
    data['name'] = name;
    data['phoneNumber'] = phoneNumber;
    data['licenceNumber'] = licenceNumber;

    return data;
  }
/*
  "memberGroup": "ROLE_ADMIN",
  "username": "string"
  "password": "string",
  "passwordRe": "string",
  "name": "string",
  "phoneNumber": "string",
  "licenceNumber": "string",
* */
}
